component extends="preside.system.base.AdminHandler" {

	property name="dao"        inject="presidecms:object:starwars_character";
	property name="messageBox" inject="messagebox@cbmessagebox";
	property name="dataManagerService" inject="dataManagerService";

	function prehandler( event, rc, prc ) {
		super.preHandler( argumentCollection = arguments );

		_checkPermissions( event=event, key="read" );

		event.addAdminBreadCrumb(
			  title = translateResource( "charactermanager:breadcrumb" )
			, link  = event.buildAdminLink( linkTo="characterManager" )
		);

		prc.pageIcon = "users";
	}

	function index( event, rc, prc ) {
		prc.pageTitle    = translateResource( "charactermanager:page.title" );
		prc.pageSubtitle = translateResource( "charactermanager:page.subtitle" );

		prc.canAdd    = hasCmsPermission( "charactermanager.add"    );
		prc.canDelete = hasCmsPermission( "charactermanager.delete" );

		prc.batchEditableFields = dataManagerService.listBatchEditableFields( "starwars_character" );
	}

	function add( event, rc, prc ) {
		_checkPermissions( event=event, key="add" );

		prc.pageTitle    = translateResource( "charactermanager:add.page.title" );
		prc.pageSubtitle = translateResource( "charactermanager:add.page.subtitle" );


		event.addAdminBreadCrumb(
			  title = translateResource( "charactermanager:add.page.breadcrumb" )
			, link  = event.buildAdminLink( linkTo="characterManager.add" )
		);
	}
	function addAction( event, rc, prc ) {
		_checkPermissions( event=event, key="add" );


		runEvent(
			  event          = "admin.DataManager._addRecordAction"
			, prePostExempt  = true
			, private        = true
			, eventArguments = {
				  object            = "starwars_character"
				, errorAction       = "characterManager.add"
				, addAnotherAction  = "characterManager.add"
				, successAction     = "characterManager"
				, redirectOnSuccess = true
				, audit             = true
				, auditType         = "charactermanager"

				, auditAction       = "add_record"
				, draftsEnabled     = false
			}
		);
	}

	function edit( event, rc, prc ) {
		_checkPermissions( event=event, key="edit" );



		var id      = rc.id ?: "";
		var version = Val( rc.version ?: "" );

		prc.record = dao.selectData(
			  filter             = { id=id }
			, fromVersionTable   = true

			, specificVersion    = version
		);

		if ( !prc.record.recordCount ) {
			messageBox.error( translateResource( uri="charactermanager:record.not.found.error" ) );
			setNextEvent( url=event.buildAdminLink( linkTo="characterManager" ) );
		}
		prc.record = queryRowToStruct( prc.record );

		prc.pageTitle    = translateResource( uri="charactermanager:edit.page.title", data=[ prc.record.name ] );
		prc.pageSubtitle = translateResource( uri="charactermanager:edit.page.subtitle", data=[ prc.record.name ] );

		event.addAdminBreadCrumb(
			  title = translateResource( uri="charactermanager:edit.page.breadcrumb", data=[ prc.record.name ] )
			, link  = event.buildAdminLink( linkTo="characterManager.edit", queryString="id=#id#" )
		);
	}
	function editAction( event, rc, prc ) {
		_checkPermissions( event=event, key="edit" );

		var id = rc.id ?: "";


		prc.record = dao.selectData( filter={ id=id } );

		if ( !prc.record.recordCount ) {
			messageBox.error( translateResource( uri="charactermanager:record.not.found.error" ) );
			setNextEvent( url=event.buildAdminLink( linkTo="characterManager" ) );
		}

		runEvent(
			  event          = "admin.DataManager._editRecordAction"
			, private        = true
			, prePostExempt  = true
			, eventArguments = {
				  object            = "starwars_character"
				, errorAction       = "characterManager.edit"
				, successUrl        = event.buildAdminLink( linkto="characterManager" )
				, redirectOnSuccess = true
				, audit             = true
				, auditType         = "charactermanager"

				, auditAction       = "edit"
				, draftsEnabled     = false
			}
		);
	}

	function deleteAction( event, rc, prc ) {
		_checkPermissions( event=event, key="delete" );

		runEvent(
			  event          = "admin.DataManager._deleteRecordAction"
			, private        = true
			, prePostExempt  = true
			, eventArguments = {
				  object       = "starwars_character"
				, postAction   = "characterManager"
				, audit        = true
				, auditType    = "charactermanager"
				, auditAction  = "delete_record"
			}
		);
	}

	function multiAction( event, rc, prc ) {
		var action = rc.multiAction ?: "";
		var ids    = rc.id          ?: "";

		switch( action ){
			case "batchUpdate":
				setNextEvent(
					  url           = event.buildAdminLink( linkTo="characterManager.batchEditField", queryString="field=#( rc.field ?: '' )#" )
					, persistStruct = { id = ids }
				);
			break;
			case "delete":
				return deleteAction( argumentCollection = arguments );
			break;
		}
		messageBox.error( translateResource( "cms:datamanager.invalid.multirecord.action.error" ) );
		setNextEvent( url=event.buildAdminLink( linkTo="characterManager" ) );
	}

	function batchEditField( event, rc, prc ) {
		var ids         = Trim( rc.id    ?: "" );
		var field       = Trim( rc.field ?: "" );
		var fieldName   = translateResource( uri="preside-objects.starwars_character:field.#field#.title", defaultValue=field );
		var recordCount = ids.listLen();

		_checkPermissions( argumentCollection=arguments, key="edit" );
		if ( !field.len() || !ids.len() ) {
			messageBox.error( translateResource( uri="charactermanager:record.not.found.error" ) );
			setNextEvent( url=event.buildAdminLink( linkTo="characterManager" ) );
		}

		prc.pageTitle    = translateResource( uri="charactermanager:batchedit.page.title", data=[ fieldName, recordCount ] );
		prc.pageSubtitle = translateResource( uri="charactermanager:batchedit.page.subtitle", data=[ fieldName, recordCount ] );

		event.addAdminBreadCrumb(
			  title = translateResource( uri="charactermanager:batchedit.page.breadcrumb", data=[ fieldName ] )
			, link  = ""
		);
	}

	function batchEditFieldAction( event, rc, prc ) {
		var updateField = rc.updateField ?: "";
		var sourceIds   = ListToArray( Trim( rc.sourceIds ?: "" ) );

		_checkPermissions( argumentCollection=arguments, key="edit" );
		if ( !sourceIds.len() ) {
			messageBox.error( translateResource( uri="charactermanager:record.not.found.error" ) );
			setNextEvent( url=event.buildAdminLink( linkTo="characterManager" ) );
		}

		var success = datamanagerService.batchEditField(
			  objectName         = "starwars_character"
			, fieldName          = updateField
			, sourceIds          = sourceIds
			, value              = rc[ updateField ]      ?: ""
			, multiEditBehaviour = rc.multiValueBehaviour ?: "append"
			, auditCategory      = "charactermanager"
			, auditAction        = "batch_edit_record"
		);

		if( success ) {
			messageBox.info( translateResource( uri="charactermanager:batchedit.confirmation" ) );
		} else {
			messageBox.error( translateResource( uri="charactermanager:batchedit.error" ) );
		}
		setNextEvent( url=event.buildAdminLink( linkTo="characterManager" ) );
	}

	public void function versionHistory( event, rc, prc ) {
		var id = rc.id ?: "";

		prc.record = dao.selectData( id=id, selectFields=[ "name" ] );
		if ( !prc.record.recordCount ) {
			messageBox.error( translateResource( uri="charactermanager:record.not.found.error" ) );
			setNextEvent( url=event.buildAdminLink( linkTo="characterManager" ) );
		}
		prc.pageTitle    = translateResource( uri="charactermanager:versionHistory.page.title"   , data=[ prc.record.name ] );
		prc.pageSubTitle = translateResource( uri="charactermanager:versionHistory.page.subTitle", data=[ prc.record.name ] );

		event.addAdminBreadCrumb(
			  title = translateResource( uri="charactermanager:versionHistory.breadcrumb"  , data=[ prc.record.name ] )
			, link  = event.buildAdminLink( linkTo="characterManager.versionHistory", queryString="id=" & id )
		);
	}

	public void function getRecordsForAjaxDataTables( event, rc, prc ) {
		_checkPermissions( event=event, key="read" );

		runEvent(
			  event          = "admin.DataManager._getObjectRecordsForAjaxDataTables"
			, prePostExempt  = true
			, private        = true
			, eventArguments = {
				  object        = "starwars_character"
				, gridFields    = "name,birth_year,gender,homeworld,film_count"
				, actionsView   = "admin.characterManager._gridActions"

			}
		);
	}

	private string function _gridActions( event, rc, prc, args={} ) {
		args.id                = args.id ?: "";
		args.deleteRecordLink  = event.buildAdminLink( linkTo="characterManager.deleteAction"  , queryString="id=" & args.id );
		args.editRecordLink    = event.buildAdminLink( linkTo="characterManager.edit"          , queryString="id=" & args.id );
		args.viewHistoryLink   = event.buildAdminLink( linkTo="characterManager.versionHistory", queryString="id=" & args.id );
		args.deleteRecordTitle = translateResource( "charactermanager:delete.record.link.title" );
		args.objectName        = "starwars_character";
		args.canEdit           = hasCmsPermission( "charactermanager.edit"   );
		args.canDelete         = hasCmsPermission( "charactermanager.delete" );
		args.canViewHistory    = hasCmsPermission( "charactermanager.view"   );

		var record = dao.selectData( id=args.id, selectFields=[ "homeworld" ] );

		args.editHomeworldLink   = event.buildAdminLink( linkTo="datamanager.editRecord", queryString="object=starwars_planet&id=" & record.homeworld );

		return renderView( view="/admin/characterManager/_gridActions", args=args );
	}

	public void function getHistoryForAjaxDatatables( event, rc, prc ) {
		var id = rc.id ?: "";

		prc.record = dao.selectData( id=id, selectFields=[ "name as label" ] );
		if ( !prc.record.recordCount ) {
			event.notFound();
		}

		runEvent(
			  event          = "admin.DataManager._getRecordHistoryForAjaxDataTables"
			, prePostExempt  = true
			, private        = true
			, eventArguments = {
				  object     = "starwars_character"
				, recordId   = id
				, actionsView = "admin/characterManager/_historyActions"
			}
		);
	}

	public void function exportDataAction( event, rc, prc ) {
		_checkPermissions( event=event, key="read" );

		runEvent(
			  event          = "admin.DataManager._exportDataAction"
			, prePostExempt  = true
			, private        = true
			, eventArguments = { objectName="starwars_character" }
		);
	}


// private utility
	private void function _checkPermissions( required any event, required string key ) {
		if ( !hasCmsPermission( "charactermanager." & arguments.key ) ) {
			event.adminAccessDenied();
		}
	}

}