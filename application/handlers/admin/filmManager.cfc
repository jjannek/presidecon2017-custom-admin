component extends="preside.system.base.AdminHandler" {

	property name="dao"        inject="presidecms:object:starwars_film";
	property name="messageBox" inject="messagebox@cbmessagebox";
	property name="dataManagerService" inject="dataManagerService";

	function prehandler( event, rc, prc ) {
		super.preHandler( argumentCollection = arguments );

		_checkPermissions( event=event, key="read" );

		event.addAdminBreadCrumb(
			  title = translateResource( "filmmanager:breadcrumb" )
			, link  = event.buildAdminLink( linkTo="filmManager" )
		);

		prc.pageIcon = "film";
	}

	function index( event, rc, prc ) {
		prc.pageTitle    = translateResource( "filmmanager:page.title" );
		prc.pageSubtitle = translateResource( "filmmanager:page.subtitle" );

		prc.canAdd    = hasCmsPermission( "filmmanager.add"    );
		prc.canDelete = hasCmsPermission( "filmmanager.delete" );

		prc.batchEditableFields = dataManagerService.listBatchEditableFields( "starwars_film" );
	}

	function add( event, rc, prc ) {
		_checkPermissions( event=event, key="add" );

		prc.pageTitle    = translateResource( "filmmanager:add.page.title" );
		prc.pageSubtitle = translateResource( "filmmanager:add.page.subtitle" );


		event.addAdminBreadCrumb(
			  title = translateResource( "filmmanager:add.page.breadcrumb" )
			, link  = event.buildAdminLink( linkTo="filmManager.add" )
		);
	}
	function addAction( event, rc, prc ) {
		_checkPermissions( event=event, key="add" );


		runEvent(
			  event          = "admin.DataManager._addRecordAction"
			, prePostExempt  = true
			, private        = true
			, eventArguments = {
				  object            = "starwars_film"
				, errorAction       = "filmManager.add"
				, addAnotherAction  = "filmManager.add"
				, successAction     = "filmManager"
				, redirectOnSuccess = true
				, audit             = true
				, auditType         = "filmmanager"

				, auditAction       = "add_record"
				, draftsEnabled     = false
			}
		);
	}

	function edit( event, rc, prc ) {
        _checkPermissions( event=event, key="edit" );

        prc.canSaveDraft = hasCmsPermission( "filmmanager.saveDraft" );
        prc.canPublish   = hasCmsPermission( "filmmanager.publish"   );
        if ( !prc.canSaveDraft && !prc.canPublish ) {
            event.adminAccessDenied()
        }

        var id      = rc.id ?: "";
        var version = Val( rc.version ?: "" );

        prc.record = dao.selectData(
              filter             = { id=id }
            , fromVersionTable   = true
            , allowDraftVersions = true
            , specificVersion    = version
        );

        if ( !prc.record.recordCount ) {
            messageBox.error( translateResource( uri="filmmanager:record.not.found.error" ) );
            setNextEvent( url=event.buildAdminLink( linkTo="filmManager" ) );
        }
        prc.record = queryRowToStruct( prc.record );

        var favorites = listToArray( getSystemSetting( "lookups", "film_favorites" ) );

        prc.favorite = favorites.findNoCase( id );

        prc.pageTitle    = translateResource( uri="filmmanager:edit.page.title", data=[ prc.record.title ] );
        prc.pageSubtitle = translateResource( uri="filmmanager:edit.page.subtitle", data=[ prc.record.title ] );

        event.addAdminBreadCrumb(
              title = translateResource( uri="filmmanager:edit.page.breadcrumb", data=[ prc.record.title ] )
            , link  = event.buildAdminLink( linkTo="filmManager.edit", queryString="id=#id#" )
        );
    }
    
	function editAction( event, rc, prc ) {
		_checkPermissions( event=event, key="edit" );

		var id = rc.id ?: "";


		prc.record = dao.selectData( filter={ id=id } );

		if ( !prc.record.recordCount ) {
			messageBox.error( translateResource( uri="filmmanager:record.not.found.error" ) );
			setNextEvent( url=event.buildAdminLink( linkTo="filmManager" ) );
		}

		runEvent(
			  event          = "admin.DataManager._editRecordAction"
			, private        = true
			, prePostExempt  = true
			, eventArguments = {
				  object            = "starwars_film"
				, errorAction       = "filmManager.edit"
				, successUrl        = event.buildAdminLink( linkto="filmManager" )
				, redirectOnSuccess = true
				, audit             = true
				, auditType         = "filmmanager"

				, auditAction       = "edit"
				, draftsEnabled     = false
			}
		);
	}

	function deleteAction( event, rc, prc ) {
		_checkPermissions( event=event, key="delete" );

		runEvent(
			  event          = "admin.DataManager._deleteRecordAction"
			, private        = true
			, prePostExempt  = true
			, eventArguments = {
				  object       = "starwars_film"
				, postAction   = "filmManager"
				, audit        = true
				, auditType    = "filmmanager"
				, auditAction  = "delete_record"
			}
		);
	}

	function multiAction( event, rc, prc ) {
		var action = rc.multiAction ?: "";
		var ids    = rc.id          ?: "";

		switch( action ){
			case "batchUpdate":
				setNextEvent(
					  url           = event.buildAdminLink( linkTo="filmManager.batchEditField", queryString="field=#( rc.field ?: '' )#" )
					, persistStruct = { id = ids }
				);
			break;
			case "delete":
				return deleteAction( argumentCollection = arguments );
			break;
		}
		messageBox.error( translateResource( "cms:datamanager.invalid.multirecord.action.error" ) );
		setNextEvent( url=event.buildAdminLink( linkTo="filmManager" ) );
	}

	function batchEditField( event, rc, prc ) {
		var ids         = Trim( rc.id    ?: "" );
		var field       = Trim( rc.field ?: "" );
		var fieldName   = translateResource( uri="preside-objects.starwars_film:field.#field#.title", defaultValue=field );
		var recordCount = ids.listLen();

		_checkPermissions( argumentCollection=arguments, key="edit" );
		if ( !field.len() || !ids.len() ) {
			messageBox.error( translateResource( uri="filmmanager:record.not.found.error" ) );
			setNextEvent( url=event.buildAdminLink( linkTo="filmManager" ) );
		}

		prc.pageTitle    = translateResource( uri="filmmanager:batchedit.page.title", data=[ fieldName, recordCount ] );
		prc.pageSubtitle = translateResource( uri="filmmanager:batchedit.page.subtitle", data=[ fieldName, recordCount ] );

		event.addAdminBreadCrumb(
			  title = translateResource( uri="filmmanager:batchedit.page.breadcrumb", data=[ fieldName ] )
			, link  = ""
		);
	}

	function batchEditFieldAction( event, rc, prc ) {
		var updateField = rc.updateField ?: "";
		var sourceIds   = ListToArray( Trim( rc.sourceIds ?: "" ) );

		_checkPermissions( argumentCollection=arguments, key="edit" );
		if ( !sourceIds.len() ) {
			messageBox.error( translateResource( uri="filmmanager:record.not.found.error" ) );
			setNextEvent( url=event.buildAdminLink( linkTo="filmManager" ) );
		}

		var success = datamanagerService.batchEditField(
			  objectName         = "starwars_film"
			, fieldName          = updateField
			, sourceIds          = sourceIds
			, value              = rc[ updateField ]      ?: ""
			, multiEditBehaviour = rc.multiValueBehaviour ?: "append"
			, auditCategory      = "filmmanager"
			, auditAction        = "batch_edit_record"
		);

		if( success ) {
			messageBox.info( translateResource( uri="filmmanager:batchedit.confirmation" ) );
		} else {
			messageBox.error( translateResource( uri="filmmanager:batchedit.error" ) );
		}
		setNextEvent( url=event.buildAdminLink( linkTo="filmManager" ) );
	}

	public void function versionHistory( event, rc, prc ) {
		var id = rc.id ?: "";

		prc.record = dao.selectData( id=id, selectFields=[ "title" ] );
		if ( !prc.record.recordCount ) {
			messageBox.error( translateResource( uri="filmmanager:record.not.found.error" ) );
			setNextEvent( url=event.buildAdminLink( linkTo="filmManager" ) );
		}
		prc.pageTitle    = translateResource( uri="filmmanager:versionHistory.page.title"   , data=[ prc.record.title ] );
		prc.pageSubTitle = translateResource( uri="filmmanager:versionHistory.page.subTitle", data=[ prc.record.title ] );

		event.addAdminBreadCrumb(
			  title = translateResource( uri="filmmanager:versionHistory.breadcrumb"  , data=[ prc.record.title ] )
			, link  = event.buildAdminLink( linkTo="filmManager.versionHistory", queryString="id=" & id )
		);
	}

	public void function getRecordsForAjaxDataTables( event, rc, prc ) {
		_checkPermissions( event=event, key="read" );

		runEvent(
			  event          = "admin.DataManager._getObjectRecordsForAjaxDataTables"
			, prePostExempt  = true
			, private        = true
			, eventArguments = {
				  object        = "starwars_film"
				, gridFields    = "title,episode_id,release_date"
				, actionsView   = "admin.filmManager._gridActions"

			}
		);
	}

	private string function _gridActions( event, rc, prc, args={} ) {
		args.id                = args.id ?: "";
		args.deleteRecordLink  = event.buildAdminLink( linkTo="filmManager.deleteAction"  , queryString="id=" & args.id );
		args.editRecordLink    = event.buildAdminLink( linkTo="filmManager.edit"          , queryString="id=" & args.id );
		args.viewHistoryLink   = event.buildAdminLink( linkTo="filmManager.versionHistory", queryString="id=" & args.id );
		args.deleteRecordTitle = translateResource( "filmmanager:delete.record.link.title" );
		args.objectName        = "starwars_film";
		args.canEdit           = hasCmsPermission( "filmmanager.edit"   );
		args.canDelete         = hasCmsPermission( "filmmanager.delete" );
		args.canViewHistory    = hasCmsPermission( "filmmanager.view"   );

		return renderView( view="/admin/filmManager/_gridActions", args=args );
	}

	public void function getHistoryForAjaxDatatables( event, rc, prc ) {
		var id = rc.id ?: "";

		prc.record = dao.selectData( id=id, selectFields=[ "title as label" ] );
		if ( !prc.record.recordCount ) {
			event.notFound();
		}

		runEvent(
			  event          = "admin.DataManager._getRecordHistoryForAjaxDataTables"
			, prePostExempt  = true
			, private        = true
			, eventArguments = {
				  object     = "starwars_film"
				, recordId   = id
				, actionsView = "admin/filmManager/_historyActions"
			}
		);
	}

	public void function exportDataAction( event, rc, prc ) {
		_checkPermissions( event=event, key="read" );

		runEvent(
			  event          = "admin.DataManager._exportDataAction"
			, prePostExempt  = true
			, private        = true
			, eventArguments = { objectName="starwars_film" }
		);
	}


// private utility
	private void function _checkPermissions( required any event, required string key ) {
		if ( !hasCmsPermission( "filmmanager." & arguments.key ) ) {
			event.adminAccessDenied();
		}
	}

}