<cfoutput>
	#renderViewlet( event="admin.datamanager._batchEditForm", args={
		  saveChangesAction = event.buildAdminLink( linkTo='filmManager.batchEditFieldAction' )
		, cancelAction      = event.buildAdminLink( linkTo="filmManager" )
		, object            = "starwars_film"
		, ids               = rc.id    ?: ""
		, field             = rc.field ?: ""
	} )#
</cfoutput>