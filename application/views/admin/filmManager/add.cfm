<cfoutput>
	#renderView( view="/admin/datamanager/_addRecordForm", args={
		  objectName            = "starwars_film"
		, addRecordAction       = event.buildAdminLink( linkTo='filmManager.addAction' )
		, cancelAction          = event.buildAdminLink( linkTo='filmManager' )

		, allowAddAnotherSwitch = true
	} )#
</cfoutput>