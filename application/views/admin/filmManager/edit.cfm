<cfscript>
	recordId = rc.id      ?: "";
	version  = rc.version ?: "";
</cfscript>

<cfoutput>
	<cfif prc.favorite>
        <div class="alert alert-info" role="alert">This is one of your favorites!</div>     
    </cfif>
	#renderViewlet( event='admin.datamanager.versionNavigator', args={
		  object         = "starwars_film"
		, id             = recordId
		, version        = version
		, isDraft        = IsTrue( prc.record._version_is_draft ?: "" )
		, baseUrl        = event.buildAdminLink( linkto="filmManager.edit", queryString="id=#recordId#&version=" )
		, allVersionsUrl = event.buildAdminLink( linkto="filmManager.versionHistory", queryString="id=#recordId#" )
	} )#

	#renderView( view="/admin/datamanager/_editRecordForm", args={
		  object           = "starwars_film"
		, id               = rc.id      ?: ""
		, record           = prc.record ?: {}
		, editRecordAction = event.buildAdminLink( linkTo='filmManager.editAction' )
		, cancelAction     = event.buildAdminLink( linkTo='filmManager' )
		, useVersioning    = true

	} )#
</cfoutput>