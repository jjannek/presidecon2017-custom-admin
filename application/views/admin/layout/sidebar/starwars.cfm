<cfscript>
      subMenuItems = [];
      if ( hasCmsPermission( "filmManager.read" ) ) {
          subMenuItems.append( {
                link  = event.buildAdminLink( linkTo="filmManager" )
              , title = translateResource( "cms:starwars.films" )
              , active = ReFindNoCase( "\.?filmManager$", event.getCurrentHandler() )
          } );
      }
      if ( hasCmsPermission( "characterManager.read" ) ) {
          subMenuItems.append( {
                link  = event.buildAdminLink( linkTo="characterManager" )
              , title = translateResource( "cms:starwars.characters" )
              , active = ReFindNoCase( "\.?characterManager$", event.getCurrentHandler() )
          } );
      }

      if ( subMenuItems.len() == 2 ) {
          WriteOutput( renderView(
                view = "/admin/layout/sidebar/_menuItem"
              , args = {
                    active       = subMenuItems[1].active || subMenuItems[2].active
                  , icon         = "fa-film"
                  , title        = translateResource( 'cms:starwars' )
                  , subMenuItems = subMenuItems
                }
          ) );
      } else if ( subMenuItems.len() == 1 ) {
          WriteOutput( renderView(
                view = "/admin/layout/sidebar/_menuItem"
              , args = {
                    active = subMenuItems[1].active
                  , title  = subMenuItems[1].title
                  , link   = subMenuItems[1].link
                  , icon   = "fa-film"
                }
          ) );
      }
</cfscript>