<cfoutput>
	#renderView( view="/admin/datamanager/_addRecordForm", args={
		  objectName            = "starwars_character"
		, addRecordAction       = event.buildAdminLink( linkTo='characterManager.addAction' )
		, cancelAction          = event.buildAdminLink( linkTo='characterManager' )

		, allowAddAnotherSwitch = true
	} )#
</cfoutput>