<cfoutput>
	#renderViewlet( event="admin.datamanager._batchEditForm", args={
		  saveChangesAction = event.buildAdminLink( linkTo='characterManager.batchEditFieldAction' )
		, cancelAction      = event.buildAdminLink( linkTo="characterManager" )
		, object            = "starwars_character"
		, ids               = rc.id    ?: ""
		, field             = rc.field ?: ""
	} )#
</cfoutput>