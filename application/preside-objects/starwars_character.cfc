/**
 * @versioned true
 */
component dataManagerGroup="" {
    property name="film_count" type="numeric" dbtype="int" formula="Count( distinct ${prefix}films.id )";
    property name="gender" renderer="gender";
}