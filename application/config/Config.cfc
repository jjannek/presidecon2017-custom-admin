component extends="preside.system.config.Config" {

	public void function configure() {
		super.configure();

		settings.preside_admin_path  = "admin";
		settings.system_users        = "sysadmin";
		settings.default_locale      = "en";

		settings.default_log_name    = "presidecon2017";
		settings.default_log_level   = "information";
		settings.sql_log_name        = "presidecon2017";
		settings.sql_log_level       = "information";

		settings.ckeditor.defaults.stylesheets.append( "css-bootstrap" );
		settings.ckeditor.defaults.stylesheets.append( "css-layout" );

		settings.features.websiteUsers.enabled = false;
		settings.features.dataexport.enabled = true;

		settings.adminPermissions[ "filmmanager"      ] = [ "add", "edit", "delete", "savedraft", "publish", "read" ];
        settings.adminPermissions[ "charactermanager" ] = [ "add", "edit", "delete", "savedraft", "publish", "read" ];

        settings.adminRoles.sysAdmin.append( "filmmanager.*"      );
        settings.adminRoles.sysAdmin.append( "charactermanager.*" );

        settings.adminRoles.starwarsManager = [ "filmmanager.*"   , "charactermanager.*"    ];
        settings.adminRoles.starwarsViewer  = [ "filmmanager.read", "charactermanager.read" ];
        
        settings.adminSideBarItems.append( "starwars" );
	}
}
